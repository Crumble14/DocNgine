#ifndef DOC_NGINE
#define DOC_NGINE

#include<string>
#include<vector>

#include<json.hpp>

#define DOCUMENTS_DIR "documents/"
#define INDEXES_DIR "indexes/"

#define BASE_FILE "base.json"

namespace DocNgine
{
	using namespace std;
	using json = nlohmann::json;

	inline string makeDirPath(const string& path)
	{
		if(path.empty()) return "/";
		if(path.back() != '/') return path + '/';

		return path;
	}

	class Document
	{
		public:
			inline Document(const string& db_directory,
				const string& path)
				: db_directory{makeDirPath(db_directory)},
				path{path}
			{}

			inline const string& getPath() const
			{
				return path;
			}

			inline string getRealpath() const
			{
				return db_directory + DOCUMENTS_DIR + path;
			}

			string getName() const;

			bool exists() const;

			inline bool isContentLoaded() const
			{
				return loaded;
			}

			json& read();
			void write(const json& content);

			void rename(const string& new_name);

			void copy(const string& path) const;
			void move(const string& new_path);

			json getField(const string& field);
			void setField(const string& field, const json& value);
			void removeField(const string& field);

			void deleteDocument();

		private:
			const string db_directory;
			const string path;

			json content;
			bool loaded = false;
	};

	class Database
	{
		public:
			inline Database(const string& directory)
				: directory{directory}
			{}

			inline const string& getDirectory() const
			{
				return directory;
			}

			Document getDocument(const string& path) const;
			vector<Document> listDocuments(const string& path,
				const bool recursive) const;

		private:
			const string directory;
	};
}

#endif
