# DocNgine

DocNgine is a database engine which allows to store data into json document format.



## Files structure

All database data are stored into a directory dedicated to it.
The structure of this directory is the following:

```
database/
	documents/
		<All the documents stored into the database>
	indexes/
		<All the index files>
	base.json
```

The **base.json** file stores the database's informations such as:
- Name
- Owner
- Creation timestamp



## Indexes

When a value is often used for a specific field, it's stored with the list of documents that contains it into an index.
This allows faster documents selection.

Here is an example of index:

```json
{
	"entries":[
		{
			"fields":{
				"foo":"bar"
			},
			"documents":[
				"/foo"
			]
		}
	]
}
```

In the previous example, when a condition checks if the field ``foo`` has the value ``"bar"``, it can refer to this index. Thus, the condition will be fullfilled for the document ``/foo``.

This avoids opening every file to check its content.

When a file is created, modified or deleted, the index is automaticly updated.

Index entries are created when a field value is present in a significative part of the database's documents.
